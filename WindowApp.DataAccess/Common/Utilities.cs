﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace WindowApp.DataAccess.Common
{
    /// <summary>
    /// utilities class has convertion methods
    /// </summary>
    public class Utilities
    {
        #region " Conversion Functions "
        
        /// <summary>
        /// Converts given bit value to boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Guid ConvertToGuid(object value)
        {
            Guid iOutput = Guid.Empty;

            try
            {
                if (Convert.IsDBNull(value) == false)
                {
                    iOutput = new Guid(Utilities.ConvertToString(value));
                }

                return iOutput;
            }
            catch
            {
                return iOutput;
            }
        }

        /// <summary>
        /// Converts given bit value to boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ConvertBitToBoolean(int value)
        {
            try
            {
                if (value == 1)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Converts given boolean value to bit value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ConvertBooleanToBit(bool value)
        {
            try
            {
                if (value)
                    return 1;
                return 0;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// Converts an object to a boolean value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ConvertToBoolean(object value)
        {
            try
            {
                if (Convert.IsDBNull(value) == false && !string.IsNullOrEmpty(value.ToString()))
                {
                    if (value.ToString() == "1" || value.ToString().ToLower() == "true")
                    {
                        return true;
                    }
                    else
                    {
                        return Convert.ToBoolean(value);
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Converts given object value to int value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ConvertToInt(object value)
        {
            int iOutput = 0;

            try
            {
                if (Convert.IsDBNull(value) == false)
                {
                    iOutput = Convert.ToInt32(value);
                }

                return iOutput;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// Converts given object value to int value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte ConvertToByte(object value)
        {
            byte iOutput = 0;

            try
            {
                if (Convert.IsDBNull(value) == false)
                {
                    iOutput = Convert.ToByte(value);
                }

                return iOutput;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// Converts given object value to decimal value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static decimal ConvertToDecimal(object value)
        {
            decimal dOutput = 0;

            try
            {
                if (Convert.IsDBNull(value) == false)
                {
                    dOutput = Convert.ToDecimal(value);
                }

                return dOutput;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// Converts given object value to double value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double ConvertToDouble(object value)
        {
            double dOutput = 0;

            try
            {
                if (Convert.IsDBNull(value) == false)
                {
                    dOutput = Convert.ToDouble(value);
                }

                return dOutput;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// Converts given object value to long value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static long ConvertToLong(object value)
        {
            long lOutput = 0;

            try
            {
                if (Convert.IsDBNull(value) == false)
                {
                    lOutput = Convert.ToInt64(value);
                }

                return lOutput;
            }
            catch
            {
                return 0;
            }
        }
        /// <summary>
        /// Converts given object to string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertToString(object value)
        {
            string sOutput = "";

            try
            {
                if (Convert.IsDBNull(value) == false)
                {
                    sOutput = Convert.ToString(value);
                }

                return sOutput;
            }
            catch
            {
                return sOutput;
            }
        }
        /// <summary>
        /// Converts given object to DateTime
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DateTime ConvertToDate(object value)
        {
            DateTime dtOutput = Convert.ToDateTime("1970-01-01");

            try
            {
                if (Convert.IsDBNull(value) == false)
                {
                    dtOutput = Convert.ToDateTime(value);
                }
                return dtOutput;
            }
            catch
            {
                return dtOutput;
            }
        }

        /// <summary>
        /// Converts given object value to float value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static float ConvertToFloat(object value)
        {
            float lOutput = 0;

            try
            {
                if (Convert.IsDBNull(value) == false && !string.IsNullOrEmpty(value.ToString()))
                {
                    lOutput = float.Parse(value.ToString());
                }

                return lOutput;
            }
            catch
            {
                return 0;
            }
        }

        public static long ConvertToEpocTime(System.DateTime dt)
        {
            DateTime unixRef = new DateTime(1970, 1, 1, 0, 0, 0);
            return (dt.Ticks - unixRef.Ticks) / 10000;
        }

        public static DateTime ConvertFromEpocTime(long timestamp)
        {
            DateTime unixRef = new DateTime(1970, 1, 1, 0, 0, 0);
            return unixRef.AddSeconds(timestamp / 1000);
        }

        #endregion " Conversion Functions "

        #region " Common Methods "

        /// <summary>
        /// Gets the exception message from the exception object
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public static string ReadException(Exception ex)
        {
            string sMessage = String.Empty;

            try
            {
                sMessage = ex.Message;

                //Read inner exception
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                    sMessage += ";" + ex.StackTrace + ", Message:" + ex.Message;
                }
                return sMessage;
            }
            catch
            {
                return "Could not read exception.";
            }
        }

        #endregion " Common Methods "

        #region "Image Resizing"

        ///// <summary>
        ///// <param name="actualImage">Actual Image to be resized</param>
        ///// <param name="imgWidth">Width of the Thumbnail Image</param>
        ///// <param name="imgHeight">Height of the Thumbnail Image</param>
        ///// <returns></returns>
        //public static string GetThumbnailImage(string relativeImageUrl, int imgWidth, int imgHeight)
        //{
        //    Image actualImage = null;
        //    Image _imgCur = null;
        //    MemoryStream imageStream = null;

        //    string thumbnailImageName = Path.GetFileNameWithoutExtension(relativeImageUrl) + "_thumbnail_" + Convert.ToString(imgHeight) + "_" + Convert.ToString(imgWidth) + Path.GetExtension(relativeImageUrl);

        //    ProductImage pi = ProductImage.Select_productImageByTitle(thumbnailImageName);
        //    {
        //        try
        //        {
        //            using (SPWeb web = SPContext.Current.Site.RootWeb)
        //            {
        //                SPFile file = web.GetFile(relativeImageUrl);

        //                if (file != null)
        //                {
        //                    byte[] imageBytes = file.OpenBinary();
        //                    imageStream = new MemoryStream(imageBytes);
        //                    actualImage = Image.FromStream(imageStream);
        //                }
        //            }
        //            decimal aspectRatio = getAspectRatio(actualImage.Height, actualImage.Width);
        //            int thumbWidth = imgWidth;
        //            int thumbHeight = imgHeight;
        //            if (actualImage.Width > actualImage.Height)
        //                thumbHeight = Convert.ToInt32(imgHeight * aspectRatio);
        //            else
        //                thumbWidth = Convert.ToInt32(imgWidth * aspectRatio);

        //            Image.GetThumbnailImageAbort thumbCallback = new Image.GetThumbnailImageAbort(ThumbnailCallback);
        //            _imgCur = actualImage.GetThumbnailImage(thumbWidth, thumbHeight, thumbCallback, IntPtr.Zero);

        //            string fileTemp = ConfigurationManager.AppSettings["ProductImageUploadPath"] + thumbnailImageName;
        //            _imgCur.Save(fileTemp);
        //        }
        //        catch (Exception ex)
        //        {
        //            Trace.TraceWarning("system exception on " + relativeImageUrl + " " + ex.Message);
        //            return string.Empty;
        //        }
        //        finally
        //        {
        //            if (actualImage != null) actualImage.Dispose();
        //            if (_imgCur != null) _imgCur.Dispose();
        //            if (imageStream != null) imageStream.Dispose();
        //        }
        //    }
        //}
        //private decimal getAspectRatio(int imgHeight, int imgWidth)
        //{
        //    decimal aspectRatio = (decimal)System.Math.Min(imgHeight, imgWidth) / System.Math.Max(imgHeight, imgWidth);
        //    return aspectRatio;
        //}
        //private static bool ThumbnailCallback()
        //{
        //    return false;
        //}

        #endregion "Image Resizing"
    }
}
