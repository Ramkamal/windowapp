﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace WindowApp.DataAccess.Common
{
    public class Configuration
    {
        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["WindowAppConnectionString"].Replace("IP", "RAMKUMAR-PC").Replace("DB", "PRACTISE").Replace("ID", "sa").Replace("PWD", "sql");
            }
        }

    }
}
