﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using WindowApp.DataAccess.DataHelper;
using System.Collections;
using System.Net;
using System.IO;
using System.Threading;
using WindowApp.DataAccess.DataAccess;

namespace WindowApp.DataAccess.Common
{
    public class SMSHelper
    {

        public static void SendSalesSms(string MobileNo, string Text)
        {
            string str;
            string[] strArrays;
            string sMobileNo1 = "91";
            string Url = "", SenderID = "", UserName = "", Password = "";

             int MCount = 1;
             DataSet sms = CommonDal.GetSmsSetup(MCount);
             if (sms != null && sms.Tables[0].Rows.Count > 0)
             {
                 Url = Convert.ToString(sms.Tables[0].Rows[0]["URL"]);
                 SenderID = Convert.ToString(sms.Tables[0].Rows[0]["SENDERID"]);
                 UserName = Convert.ToString(sms.Tables[0].Rows[0]["USERNAME"]);
                 Password = Convert.ToString(sms.Tables[0].Rows[0]["PASSWORD"]);

             }

            WebClient webClient = new WebClient();
            strArrays = new string[] { Url + "?username=" + UserName + "&password=" + Password + "&to=", sMobileNo1, MobileNo, "&senderid=" + SenderID + "&text=" + Text };
            str = string.Concat(strArrays);
            Stream stream = webClient.OpenRead(str);
            StreamReader streamReader = new StreamReader(stream);
            string end = streamReader.ReadToEnd();
            stream.Close();
            streamReader.Close();
              
        }
        public static void ASDSendSMS(string MobileNo, string Text)
        {
            string str;
            string[] strArrays;
            string sMobileNo1 = "91";
            string Url = "", SenderID = "", UserName = "", Password = "";

            int MCount = 1;
            DataSet sms = CommonDal.GetSmsSetupWindow(MCount);
            if (sms != null && sms.Tables[0].Rows.Count > 0)
            {
                Url = Convert.ToString(sms.Tables[0].Rows[0]["SMSURL"]);
                SenderID = Convert.ToString(sms.Tables[0].Rows[0]["SMSSENDERID"]);
                UserName = Convert.ToString(sms.Tables[0].Rows[0]["SMSUSERNAME"]);
                Password = Convert.ToString(sms.Tables[0].Rows[0]["SMSPASSWORD"]);

            }

            WebClient webClient = new WebClient();
            strArrays = new string[] { Url + "?username=" + UserName + "&password=" + Password + "&to=", sMobileNo1, MobileNo, "&senderid=" + SenderID + "&text=" + Text };
            str = string.Concat(strArrays);
            Stream stream = webClient.OpenRead(str);
            StreamReader streamReader = new StreamReader(stream);
            string end = streamReader.ReadToEnd();
            stream.Close();
            streamReader.Close();
        }
    }
}
