﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace WindowApp.DataAccess.Common
{
    public static class SecurityHelper
    {
        // Generates a Hash and Salt for a given input value.  
        // These two values should be stored in permanent storage (like the DB).  
        // As with the underlying API, a minimum value of 8 characters is required.

        public static bool GetHashAndSalt(string Original, out string Hash, out string Salt)
        {
            bool _bRet = false;
            try
            {
                if (Original == null)
                    Original = string.Empty;

                Salt = SecurityHelper.GenerateSalt();
                _bRet = SecurityHelper.PasswordHash_Salt(Original, Salt, out Hash);
            }
            catch (Exception ce)
            {
                throw ce;
            }
            return _bRet;
        }

        // Takes a previously generated Hash and Salt and will perform the same operation to the given ValueToCheck.  If the result 
        // of the hash of the new value is the same at the old, then the function will return true (the new ValueToCheck and the 
        // Original string were the same) otherwise, false indicates that they are not the same.

        public static bool CompareAgainstHashAndSalt(string ValueToCheck, string Hash, string Salt)
        {
            string pwdRet = string.Empty;
            if (SecurityHelper.PasswordHash_Salt(ValueToCheck, Salt, out pwdRet))
                return (Hash == pwdRet);
            else
                return false;
        }

        public static string PasswordHash(string cleanString)
        {
            var clearBytes = new UnicodeEncoding().GetBytes(cleanString);
            var hashedBytes = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(clearBytes);
            return BitConverter.ToString(hashedBytes);
        }
        //Call this method to generate a hashed password using MD5 algorithm that contains both password and salt value combined together
        private static bool PasswordHash_Salt(string cleanString, string salt, out string pwdRet)
        {
            pwdRet = string.Empty;
            if ((cleanString.Length >= 6) &&
               (salt.Length >= 8)
               )
            {
                string _strClearPwd = salt.Substring(0, 3) + cleanString.Substring(0, 2) + salt.Substring(3, 2) + cleanString.Substring(3) + salt.Substring(5);
                Byte[] clearBytes = new UnicodeEncoding().GetBytes(_strClearPwd);
                Byte[] hashedBytes = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(clearBytes);
                pwdRet = BitConverter.ToString(hashedBytes);
                return true;
            }
            return false;
        }

        //Call this method to generate a random salt string
        private static string GenerateSalt()
        {
            return PasswordHash(GenerateRandomCodeUsingGuid());
        }

        private static string GenerateRandomCodeUsingGuid()
        {
            return System.Guid.NewGuid().ToString().Replace("-", String.Empty);
        }

        public static string GenerateRandomPassword(int length)
        {
            var random = new Random(DateTime.Now.Millisecond);
            var newPassword = string.Empty;
            for (var i = 0; i < length; i++)
            {
                var toBeChar = random.Next(65, 90);
                if (random.Next() % 2 == 1) // 50-50 chance
                    toBeChar += 32;
                newPassword += (char)(toBeChar);
            }
            return newPassword;
        }
    }
}
