﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowApp.DataAccess.DataHelper;
using WindowApp.DataAccess.Common;
using System.Data.SqlClient;

namespace WindowApp.DataAccess.DataAccess
{
    public class CommonDal
    {
        public static DataSet getData(int Mode)
        {
            DataSet ds = new DataSet();
            try
            {
                using(DataAccessHelper dataAccessHelper = new DataAccessHelper(Configuration.ConnectionString))
                {
                    SqlParameter[] sqlParams =
                    {
                        dataAccessHelper.MakeInParam("@MODE",SqlDbType.Int,Mode),
                    };
                    ds = dataAccessHelper.ExecuteDataSet("asp_GET_EmployeeDetails", sqlParams);
                }
            }
            catch(Exception ex)
            {
                ds = null;
                throw ex;
            }
            return ds;
        }

        public static DataSet GetSmsSetup(int Mode)
        {
            DataSet ds = new DataSet();
            try
            {
                //Initialize the data access helper
                using (DataAccessHelper dataAccessHelper = new DataAccessHelper(Configuration.ConnectionString))
                {
                    SqlParameter[] sqlParams =  {
                                                    dataAccessHelper.MakeInParam("@Mode",SqlDbType.Int,Mode),
                                                };
                    ds = dataAccessHelper.ExecuteDataSet("asp_Save_SMSSetup", sqlParams);

                }
            }
            catch (Exception ex)
            {
                ds = null;
                //Log and throw the exception
                throw ex;
            }
            return ds;
        }

        public static DataSet GetSmsSetupWindow(int Mode)
        {
            DataSet ds = new DataSet();
            try
            {
                //Initialize the data access helper
                using (DataAccessHelper dataAccessHelper = new DataAccessHelper(Configuration.ConnectionString))
                {
                    SqlParameter[] sqlParams =  {
                                                    dataAccessHelper.MakeInParam("@Mode",SqlDbType.Int,Mode),
                                                };
                    ds = dataAccessHelper.ExecuteDataSet("asp_UpdSav_SMSSetup", sqlParams);

                }
            }
            catch (Exception ex)
            {
                ds = null;
                //Log and throw the exception
                throw ex;
            }
            return ds;
        }
    }

    public class CommonMas
    {
        public List<EM> EmployeeMas { get; set; }
    }

    public class EM
    {
        public string EmployeeName { get; set; }
    }
}