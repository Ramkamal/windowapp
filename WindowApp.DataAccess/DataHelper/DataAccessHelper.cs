﻿#region " Usings "

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WindowApp.DataAccess.Common;

#endregion " Usings "

namespace WindowApp.DataAccess.DataHelper
{
    /// <summary>
    /// ADO.NET data access using the SQL Server Managed Provider.
    /// </summary>
    public class DataAccessHelper : IDisposable
    {

        #region " Instance Variable "

        // connection to data source
        private SqlConnection _connection;
        private string _connectionString;

        #endregion " Instance Variable "

        public DataAccessHelper(string connectionString)
        {
            _connectionString = connectionString;
        }

        #region " Run Stored Procedure "

        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="procName">Name of stored procedure.</param>
        /// <returns>Stored procedure return value.</returns>
        public int RunProc(string procName)
        {
            this.Open();
            SqlCommand cmd = CreateCommand(procName, null);
            int rowsAffected = cmd.ExecuteNonQuery();
            this.Close();
            return rowsAffected;
        }
        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="procName">Name of stored procedure.</param>
        /// <param name="prams">Stored procedure params.</param>
        /// <returns>Stored procedure return value.</returns>
        public int RunProc(string procName, SqlParameter[] prams)
        {
            this.Open();
            SqlCommand cmd = CreateCommand(procName, prams);
            int rowsAffected = cmd.ExecuteNonQuery();
            this.Close();
            return rowsAffected;
        }

        public int RunProcRetValue(string procName, SqlParameter[] prams)
        {
            this.Open();
            SqlCommand cmd = CreateCommand(procName, prams);
            cmd.Parameters.Add(
                                new SqlParameter("@Success", SqlDbType.Int, 4, ParameterDirection.ReturnValue, false, 0, 0, string.Empty, DataRowVersion.Default, null));
            cmd.ExecuteNonQuery();
            int rowsAffected = Utilities.ConvertToInt(cmd.Parameters[cmd.Parameters.Count - 1].Value);
            this.Close();
            return rowsAffected;
        }

        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="procName">Name of stored procedure.</param>
        /// <param name="dataReader">Return result of procedure.</param>
        public void RunProc(string procName, out SqlDataReader dataReader)
        {
            this.Open();
            SqlCommand cmd = CreateCommand(procName, null);
            dataReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        }
        /// <summary>
        /// Run stored procedure.
        /// </summary>
        /// <param name="procName">Name of stored procedure.</param>
        /// <param name="prams">Stored procedure params.</param>
        /// <param name="dataReader">Return result of procedure.</param>
        public void RunProc(string procName, SqlParameter[] prams, out SqlDataReader dataReader)
        {
            this.Open();
            SqlCommand cmd = CreateCommand(procName, prams);
            dataReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        }

        public DataSet ExecuteDataSet(string procName, SqlParameter[] prams)
        {
            this.Open();
            SqlCommand cmd = CreateCommand(procName, prams);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
             
            da.Fill(ds);
            Close();
            return ds;
        }

        public DataSet ExecuteDataSet(string procName)
        {
            this.Open();
            SqlCommand cmd = CreateCommand(procName, null);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();

            da.Fill(ds);
            Close();
            return ds;
        }


        public DataTable ExecuteDataTable(string procName, SqlParameter[] prams)
        {
            this.Open();
            DataTable dt = ExecuteDataSet(procName, prams).Tables[0];
            Close();
            return dt;
        }

        public object ExecuteScalar(string procedureName, SqlParameter[] prams)
        {
            this.Open();
            SqlCommand cmd = CreateCommand(procedureName, prams);
            object result = cmd.ExecuteScalar();
            Close();
            return result;
        }

        #endregion " Run Stored Procedure "

        #region " Make Param Methods "

        /// <summary>
        /// Make input param.
        /// </summary>
        /// <param name="ParamName">Name of param.</param>
        /// <param name="DbType">Param type.</param>
        /// <param name="Size">Param size.</param>
        /// <param name="Value">Param value.</param>
        /// <returns>New parameter.</returns>
        public SqlParameter MakeInParam(string ParamName, SqlDbType DbType, int Size, object Value)
        {
            return MakeParam(ParamName, DbType, Size, ParameterDirection.Input, Value);
        }
        /// <summary>
        /// Make input param.
        /// </summary>
        /// <param name="ParamName">Name of param.</param>
        /// <param name="DbType">Param type.</param>
        /// <param name="Size">Param size.</param>
        /// <returns>New parameter.</returns>
        public SqlParameter MakeOutParam(string ParamName, SqlDbType DbType, int Size)
        {
            return MakeParam(ParamName, DbType, Size, ParameterDirection.Output, null);
        }

        /// <summary>
        /// Make input param.
        /// </summary>
        /// <param name="ParamName"></param>
        /// <param name="DbType"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public SqlParameter MakeInParam(string ParamName, SqlDbType DbType, object Value)
        {
            return MakeParam(ParamName, DbType, 0, ParameterDirection.Input, Value);
        }

        /// <summary>
        /// Make output param.
        /// </summary>
        /// <param name="ParamName"></param>
        /// <param name="DbType"></param>
        /// <returns></returns>
        public SqlParameter MakeOutParam(string ParamName, SqlDbType DbType)
        {
            return MakeParam(ParamName, DbType, 0, ParameterDirection.Output, null);
        }

        /// <summary>
        /// Make stored procedure param.
        /// </summary>
        /// <param name="ParamName">Name of param.</param>
        /// <param name="DbType">Param type.</param>
        /// <param name="Size">Param size.</param>
        /// <param name="Direction">Parm direction.</param>
        /// <param name="Value">Param value.</param>
        /// <returns>New parameter.</returns>
        public SqlParameter MakeParam(string ParamName, SqlDbType DbType, Int32 Size,
         ParameterDirection Direction, object Value)
        {
            SqlParameter param = Size > 0 ? new SqlParameter(ParamName, DbType, Size) : new SqlParameter(ParamName, DbType);

            param.Direction = Direction;
            if (!(Direction == ParameterDirection.Output && Value == null))
                param.Value = Value;

            //checking for datatype datetime and passing the min date value if value is null or empty
            if (DbType.Equals(SqlDbType.DateTime))
            {
                if (Utilities.ConvertToDate(Value).Equals(DateTime.MinValue))
                {
                    param.Value = DBNull.Value;
                }
            }

            if (DbType.Equals(SqlDbType.VarChar))
            {
                if (Utilities.ConvertToString(Value).Contains(";"))
                {
                    throw new ArgumentException("Invalid data passed into parameter");
                }
            }


            return param;
        }

        #endregion " Make Param Methods "

        #region " Public Methods "

        /// <summary>
        /// Close the connection.
        /// </summary>
        public void Close()
        {
            if (_connection != null)
                _connection.Close();
        }
        /// <summary>
        /// Release resources.
        /// </summary>
        public void Dispose()
        {
            // make sure connection is closed
            if (_connection != null)
            {
                _connection.Dispose();
                _connection = null;
            }
        }

        #endregion " Public Methods "

        #region " Helper Methods "

        /// <summary>
        /// Open the connection.
        /// </summary>
        private void Open()
        {
            //SqlConnection.ClearAllPools();
            // open connection
            if (_connection == null)
            {
                _connection = new SqlConnection(_connectionString);
                _connection.Open();
            }
            else if (_connection != null && _connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
        }
        /// <summary>
        /// Create command object used to call stored procedure.
        /// </summary>
        /// <param name="procName">Name of stored procedure.</param>
        /// <param name="prams">Params to stored procedure.</param>
        /// <returns>Command object.</returns>
        private SqlCommand CreateCommand(string procName, SqlParameter[] prams)
        {
            // make sure connection is open
            Open();


            SqlCommand cmd = new SqlCommand(procName, _connection);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 600;
            // add proc parameters
            if (prams != null)
            {
                foreach (SqlParameter parameter in prams)
                    cmd.Parameters.Add(parameter);
            }

            // return param
            //cmd.Parameters.Add(
            //    new SqlParameter("ReturnValue", SqlDbType.Int, 4,
            //    ParameterDirection.ReturnValue, false, 0, 0,
            //    string.Empty, DataRowVersion.Default, null));

            return cmd;
        }

        #endregion " Helper Methods "

        #region " Usage Code "

        /**
        public SqlDataReader GetSomething(int iUsers)
        {
            // create data object and params
            SqlDataReader dataReader = null;

            // create params for stored procedure call
            Database data = new Database();

            SqlParameter[] prams =
            {                     
                data.MakeInParam("@iUsers" ,SqlDbType.Int, 4, iUsers)
            };

            // run the stored procedure
            data.RunProc("procGetSomething", prams, out dataReader);

            return dataReader;
        }
        **/

        #endregion " Usage Code "
    }
}
