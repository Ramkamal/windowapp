﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowApp.DataAccess.DataAccess;

namespace WindowApp.Forms
{
    public partial class EmployeeMaster : Form
    {
        public EmployeeMaster()
        {
            InitializeComponent();
        }

        private void EmployeeMaster_Load(object sender, EventArgs e)
        {
            getDetails();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void getDetails()
        {
            try
            {
                dgvEmployeeDetails.DataSource = null;
                DataSet ds = CommonDal.getData(1);
                if(ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    dgvEmployeeDetails.DataSource = ds.Tables[0];

                    for(int i = 0; i < dgvEmployeeDetails.Columns.Count; i++)
                    {
                        dgvEmployeeDetails.Columns[i].Width = 150;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}