﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowApp.DataAccess.DataAccess;
using WindowApp.DataAccess.Common;
using WindowApp.DataAccess.DataHelper;
using System.Data.SqlClient;
using System.Data;
using WebApp.Models;
using Microsoft.AspNet.Identity.Owin;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult EmployeeMaster()
        {
            ViewBag.Message = "Employee Master";
            //int Mode = 1;
            //var Users = new List<EM>();
            //DataSet ds = new DataSet();
            //using(DataAccessHelper dataAccessHelper = new DataAccessHelper(Configuration.ConnectionString))
            //{
            //    SqlParameter[] sqlParams =
            //    {
            //        dataAccessHelper.MakeInParam("@MODE",System.Data.SqlDbType.Int,Mode),
            //    };
            //    ds = dataAccessHelper.ExecuteDataSet("asp_GET_EmployeeDetails", sqlParams);
            //}
            return View(DbContext.Employee.ToList());
        }

        //public EmployeeController(ApplicationDbContext dbContext)
        //{
        //    _dbcontext = dbContext;
        //}

        private ApplicationDbContext _dbcontext;

        public ApplicationDbContext DbContext
        {
            get
            {
                return _dbcontext ?? HttpContext.GetOwinContext().Get<ApplicationDbContext>();
            }
            private set
            {
                _dbcontext = value;
            }
        }
    }
}