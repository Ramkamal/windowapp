﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Models
{
    public class Employee
    {
        [Display(Name = "Employee ID")]
        public int EmployeeId { get; set; }

        [Display(Name ="Employee Name")]
        public string EmployeeName { get; set; }

        [Display(Name ="Gender")]
        public string Gender { get; set; }

        [Display(Name ="Mobile No")]
        public string MobileNo { get; set; }

        [Display(Name ="Email")]
        public string EmailId { get; set; }
    }
}